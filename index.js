var debug = require('debug')('error-helper')
var util = require('util');

const HTTP_OK			= 200,
	BAD_REQUEST		= 400,
	REZ_NOT_FOUND	= 404,	
	INTERNAL_ERROR	= 500;

const modelErrorCodes= {
    '_default_'			: 10000,
    'makes_models'		: 11000,
    'base_equipments'	: 11010,
    'make_equipments'	: 11011,
    'makes'				: 11001,
    'model_trims'		: 11030,
    'orders'			: 12000
};	
	
/*----------------------------------------------*/
/* Error Definitions...							*/
/*----------------------------------------------*/
var CustomError= function(errContent){
	Error.call(this);
	Error.captureStackTrace(this, arguments.callee);
		
	this.name = errContent.name;
	this.code= errContent.code || 1;
	this.statusCode= errContent.statusCode;
	this.message = errContent.message;
	this.context = errContent.context || {};
};
		
var ExpressValidationError= function(message){
	Error.call(this);
	Error.captureStackTrace(this, arguments.callee);
	
	this.name= 'ExpressValidationError';
	this.message= message;
	this.code= 1;
	this.statusCode= BAD_REQUEST;	
};

var MongoErr= function(err, model){

	Error.call(this);
		
	this.name= 'DuplicateError';
	this.code= modelErrorCodes[model] || 11000;
	this.message = err.message;
	this.extra= err.extra || [];
};

util.inherits(CustomError, Error);
util.inherits(ExpressValidationError, Error);
util.inherits(MongoErr, Error);

/*----------------------------------------------*/
/* Utility functions...							*/
/*----------------------------------------------*/
var _setResponseCode= function(err, data){
	
	var resCode= HTTP_OK;
	
	if (err){
		if (err instanceof ExpressValidationError || err instanceof CustomError)
			resCode= (err.statusCode) ? err.statusCode : BAD_REQUEST;
		else
			resCode= INTERNAL_ERROR;
	}
	else {
		// No err but handle resource not found...
		resCode= data ? HTTP_OK : REZ_NOT_FOUND;
	}
	
	return resCode;
};

/*----------------------------------------------*/
var handleError= function (err, modelName){	
	// Test duplicate key return code from MongoDB and return it in the expected format...
	return (11000 === err.code || 11001 === err.code) ? new MongoErr(err, modelName) : err;
};

/*----------------------------------------------*/
var handleResultFn= function(err, res, next, data){
	var resCode= _setResponseCode(err, data);

	if (err) debug('err=', err);
	
	err ? res.status(resCode).json(err) : res.status(resCode).json(resCode == REZ_NOT_FOUND ? {"message": "Resource not found"} : data);
};

/*----------------------------------------------*/
/* Export section...							*/
/*----------------------------------------------*/
exports.MongoErr= MongoErr;
exports.CustomError= CustomError;
exports.ExpressValidationError= ExpressValidationError;

// Error handling funcs...
exports.handleError= handleError;
exports.handleResultFn= handleResultFn;